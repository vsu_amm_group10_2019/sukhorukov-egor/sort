﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ext_sort
{
    public partial class MainForm : Form
    {
        string file_Name="";
        const int kol_vo = 10;
        const int mnogo = 50;
        const int FilesCount = 4;
        string input = "Пример.txt";
        public MainForm()
        {
            InitializeComponent();
        }

        private void uslovie_Click(object sender, EventArgs e)
        {//что надо сдлеать
            MessageBox.Show("Вывести названия конфет и их изготовителей в порядке увеличения срокахранения конфет." +
                "\nИспользовать многопутевое однофазное естественное несбалансированное слияние.",
                "Задача 1e",
                MessageBoxButtons.OK,
                MessageBoxIcon.Question);

        }
        
        private void CreateMenu_Click(object sender, EventArgs e)
        {//создание файла
            using (FileForm file = new FileForm(input))
            {
                DialogResult result = file.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена создания файла.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное имя файла. Файл не создан.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (!File.Exists(file.fileName) ||
                    MessageBox.Show("Файл существует. Перезаписать?", "Warning", 
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    StreamWriter newFile = new StreamWriter(file.fileName);
                    newFile.Close();
                    MessageBox.Show("Файл создан.", "Information",
                                MessageBoxButtons.OK,  MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Отмена создания файла.", "Warning",
                                MessageBoxButtons.OK,   MessageBoxIcon.Warning);
            }

        }

        private void OpenMenu_Click(object sender, EventArgs e)
        {//открытие существующего файла
            using (FileForm file = new FileForm(input))
            {
                DialogResult result = file.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена создания файла.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное имя файла. Файл не открыт.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error); 
                else if (!File.Exists(file.fileName))
                    MessageBox.Show("Файл не существует.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    file_Name = file.fileName;                    
                    intxt.LoadFile(file_Name, RichTextBoxStreamType.PlainText);
                    outtxt.Text = "";
                    MessageBox.Show("Файл открыт.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        
        private void SaveMenu_Click(object sender, EventArgs e)
        {//сохранение файла
            if (file_Name != "")
            {
                intxt.SaveFile(file_Name, RichTextBoxStreamType.PlainText);
                MessageBox.Show("Файл сохранён.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Файл не сохранён, т.к. он не открыт.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void AddMenu_Click(object sender, EventArgs e)
        {//добавление в открытый файл
            if (file_Name == "")
            {
                MessageBox.Show("Необходимо открыть файл.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            using (CandiesForm file = new CandiesForm())
            {
                DialogResult result = file.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена добавления элемента.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное значение элемента. Элемент не добавлен.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Candies candy = file.Candy;
                    intxt.Text += candy.ToString() + Environment.NewLine;
                    MessageBox.Show("Элемент добавлен.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }
        private void AddTenMenu_Click(object sender, EventArgs e)
        {//генерация 10 записей
            if (file_Name == "")
            {
                MessageBox.Show("Необходимо открыть файл.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fileStream = File.OpenWrite(file_Name);
            for (int i=0; i<kol_vo; i++)
            {
                CandiesForm file = new CandiesForm(kol_vo);
               
                Candies candy = file.Candy;               

                intxt.Text += candy.ToString() + Environment.NewLine;
                formatter.Serialize(fileStream, candy);
            }
            fileStream.Close();
        }
        private void Add50Menu_Click(object sender, EventArgs e)
        {//генерация 50 записей
            if (file_Name == "")
            {
                MessageBox.Show("Необходимо открыть файл.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fileStream = File.OpenWrite(file_Name);
            for (int i = 0; i < mnogo; i++)
            {
                CandiesForm file = new CandiesForm(mnogo);

                Candies candy = file.Candy;

                intxt.Text += candy.ToString() + Environment.NewLine;
                formatter.Serialize(fileStream, candy);
            }
            fileStream.Close();

        }

        private void SortMenu_Click(object sender, EventArgs e)
        {//сортировка открытого файла
            if (file_Name == "")
                MessageBox.Show("Необходимо открыть файл.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                outtxt.Text = "";
                Sort sort = new Sort();
                sort.SortFile(file_Name);
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream fileStream = File.OpenRead(file_Name);
                while (fileStream.Position < fileStream.Length)
                {
                    Candies candies = (Candies)formatter.Deserialize(fileStream);
                    outtxt.Text += candies.ToString() + "\n";
                }
                fileStream.Close();
            }
        }        
        /// <summary>
        /// записываем в файл последний считанный элемент
        /// </summary>
        /// <param name="f_in"> файл, из которого считан lastRead</param>
        /// <param name="f_out"> файл, в который записываем lastRead</param>
        /// <param name="lastRead"> последний считанный элемент из файла </param>
        /// <returns> возвращаем true, если в файле, из которого считали элемент, продолжается текущая серия </returns>
        bool AddFromFile(StreamReader f_in, StreamWriter f_out, ref Candies lastRead)
        {
            f_out.WriteLine(lastRead); // записываем последний считанный элемент в файл
            Candies lastWritten = lastRead; // запоминаем последний записанный элемент
            string info = f_in.ReadLine(); // считывает следующий элемент из файла file_read
            if (info == null)
            {
                lastRead = null;
                return false; 
            }            
            lastRead = new Candies(info);            
            return lastRead.CompareTo(lastWritten) >= 0;
        }

        
    }
}
