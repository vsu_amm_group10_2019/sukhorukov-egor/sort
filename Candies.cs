﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ext_sort
{
    [Serializable]
    public class Candies : IComparable
    {
        private static Random rnd = new Random();
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Price { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateOfManufacture { get; set; }
        public int expirationDate { get; set; }

        public Candies()
        {
            Name = "—";
            Weight = 0;
            Price = 0;
            Manufacturer = "—";
            DateOfManufacture = DateTime.Parse("2021.01.01");
            expirationDate = 0;
        }
        public Candies(string information)
        {
            string[] info = information.Split();
            info = info.Where(x => x != "").ToArray();
            Name = info[0];
            Weight = Convert.ToInt32(info[1]);
            Price = Convert.ToInt32(info[2]);
            Manufacturer = info[3];
            DateOfManufacture = DateTime.Parse(info[4]);
            expirationDate = Convert.ToInt32(info[5]);
        }

        public override string ToString()
        {//вывод в форму
            return Name.PadRight(10) + Weight.ToString().PadRight(10) +
                Price.ToString().PadRight(10) + Manufacturer.PadRight(10) + 
                DateOfManufacture.ToShortDateString().PadRight(25) +expirationDate.ToString();
        }

        public int CompareTo(object obj)
        { //возврат значения для сравнения
            Candies candy = (Candies)(obj);
            return Name.CompareTo(candy.Name);
        }

        private string GenerateString()
        {
            return Guid.NewGuid().ToString();
        }

        private DateTime GenerateDate()
        {
            DateTime date = new DateTime(2020, 1, 1);
            return date.AddDays(rnd.Next(750));
        }
        public void GenerateRandom()
        {//автозаполнение
            Name = GenerateString();
            Weight = 1 + rnd.Next(1000);
            Price = 1 + rnd.Next(1000);
            Manufacturer = GenerateString();
            DateOfManufacture = GenerateDate();
            expirationDate = 1 + rnd.Next(1000);
        }
    }
}
