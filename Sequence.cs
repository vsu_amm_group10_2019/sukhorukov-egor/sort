﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ext_sort
{
    public class Sequence : IDisposable
    {
        private  BinaryFormatter Formatter { get; set; }
        private FileStream FileStream { get; set; }
        public FileInfo FileInfo { get; }
        public Candies Element { get; private set; }
        public bool ReadyToCopy { get; private set; }
        private int Remaning { get; set; }
        public Sequence(string filePath)
        {
            FileInfo = new FileInfo(filePath);
            Formatter = new BinaryFormatter();
        }
        public void NewRun(int length)
        {
            Remaning = length;
        }
        public void StartRead()
        {
            FileStream = File.OpenRead(FileInfo.FullName);
        }
        public void StopRead()
        {
            FileStream.Close();
        }
        public void StartWrite()
        {
            FileStream = File.OpenWrite(FileInfo.FullName);
        }
        public void StopWrite()
        {
            FileStream.Close();
        }
        public void WriteElem(Candies element)
        {
            Formatter.Serialize(FileStream, element);
        }
        public bool IsEndOfFile()
        {
            return FileStream.Position == FileStream.Length;
        }
        public bool IsEndOfSeries()
        {
            return IsEndOfFile() || Remaning == 0;
        }
        public void Dispose()
        {
            FileStream?.Dispose();
        }
        public void ReadElem()
        {
            if (!IsEndOfSeries())
            {
                Element = (Candies)Formatter.Deserialize(FileStream);
                ReadyToCopy = true;
            }
        }
        public void CopyElementTo(Sequence sequence)
        {
            if (ReadyToCopy)
            {
                sequence.WriteElem(Element);
                Remaning--;
                ReadyToCopy = false;
            }
        }
        public void CopyAllTo(Sequence sequence)
        {
            ReadElem();
            do
            {
                CopyElementTo(sequence);
                ReadElem();
            } while (ReadyToCopy);
        }
    }
}
